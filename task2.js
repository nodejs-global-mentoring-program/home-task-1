import csv from 'csvtojson';
import fsPromise from 'fs/promises';
import fs from 'fs';

(async () => {
  console.log('Reading csv using csvtojson');
  const jsonArray = await csv().fromFile('./csv/input.csv');
  console.log('Finish reading');
  let data = '';
  for (const json of jsonArray) {
    data += JSON.stringify(json) + '\n';
  }
  await fsPromise.writeFile('output.txt', data);
  console.log('First txt file generated - output.txt\n');

  const stream = fs.createReadStream('./csv/input.csv');
  let chunks = [];
  let rawInput;
  console.log('Reading csv using stream')
  stream.on("error", function(error) { console.error(error) });
  stream.on("data", function(data) { chunks.push(data) });
  stream.on("end", async function() {
    console.log('Finish reading');
    rawInput = chunks.toString();
    let lines = rawInput.split(/\r?\n/);
    const headers = lines[0].split(',');
    lines = lines.slice(1);
    let data2 = '';
    for (const line of lines) {
      const entries = line.split(',');
      let lineToWrite = '{';
      for (let j = 0; j < headers.length - 1; j++) {
        lineToWrite += `"${headers[j]}":"${entries[j]}",`;
      }
      lineToWrite += `"${headers[headers.length - 1]}":"${entries[headers.length - 1]}"`;
      data2 += lineToWrite + '}\n';
    }
    await fsPromise.writeFile('output2.txt', data2);
    console.log('Second txt file generated - output2.txt\n');
  });
  
})();

