import readline from 'readline';

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const processInput = () => {
  rl.question('\n', (input) => {
    let reversedInput = '';
    for (let index = input.length - 1; index >= 0; index--) {
      reversedInput += input[index];
    }
    console.log(reversedInput);
    processInput();
  });
}

processInput();
